#### CLI para consultar informações sobre gastos de deputados

##### Pré-requisitos
[Instalar o pip e virtualenv](https://pythonacademy.com.br/blog/python-e-virtualenv-como-programar-em-ambientes-virtuais)


##### Configurar o projeto

Com o pip e virtualenv já instalados, execute o comando abaixo para criar o virtualenv e instalar as dependências.

    $ ./configure.sh
 

Ou [Configure o virtual-environment no Pycharm](https://www.jetbrains.com/help/pycharm/creating-virtual-environment.html)






##### Comandos

    $ python ./main.py --help
    Usage: main.py [OPTIONS] COMMAND [ARGS]...
    
      CLI para consultar informações sobre gastos de candidatos
    
    Options:
      --help  Show this message and exit.
    
    Commands:
      buscar    Filtrar candidatos
      detalhar  Detalha os gastos de um deputado a partir de seu número...

##### Buscar candidados

    $ python ./main.py buscar --help
    Usage: main.py buscar [OPTIONS]
    
      Filtrar candidatos
    
    Options:
      -a, --ano INTEGER   Ano da apuração dos dados
      -n, --nome TEXT     Nome completo do candidato ou parte dele
      -p, --partido TEXT  Sigla do partido
      --help              Show this message and exit.


##### Detalhar candidado

    $ python ./main.py detalhar --help
    Usage: main.py detalhar [OPTIONS] ID
    
      Detalha os gastos de um deputado a partir de seu número nuDeputadoId
    
    Options:
      -a, --ano INTEGER  Ano da apuração dos dados
      --help             Show this message and exit.

