import pathlib
import json
import utils
import pydash as _

data_path = pathlib.Path("data")


def create_category(category):
    key = utils.param_case(category)
    return {key: category}


def items_descriptions(year, overwrite=False):
    filename = data_path / f"gastos_depudatados_{year}.json"

    if filename.exists() is not True and overwrite is not True:
        with json.load(filename) as categories:
            return categories

    origin_data = utils.read_json_file(filename)
    outfile = open(data_path / f"categorias_gastos_{year}.txt", 'w')

    categories = _.map_(_.uniq("\n".join(_.uniq(_.map_(origin_data, lambda values, key: "\n".join(
        _.uniq(_.map_(values['despesas'], lambda item: item['txtDescricao'])))))).split("\n")),
                        lambda category: create_category(category))
    json.dump(categories, outfile, ensure_ascii=False)
    outfile.close()
    return categories

# items_descriptions(2018)
