import click
import dep

@click.group()
def main():
    """
    CLI para consultar informações sobre gastos de deputados
    """
    pass

@main.command()
@click.option('--ano', '-a', default=2018, help="Ano da apuração dos dados", type=int)
@click.option('--nome', '-n', help="Nome completo do deputado ou parte dele", type=str)
@click.option('--partido', '-p', help="Sigla do partido", type=str)
def buscar(ano, nome, partido):
    """Filtrar deputados"""
    click.echo(dep.find(ano, nome, partido))

@main.command()
@click.argument('id', type=int)
@click.option('--ano', '-a', default=2018, help="Ano da apuração dos dados", type=int)
def detalhar(id, ano):
    """Detalha os gastos de um deputado a partir de seu número nuDeputadoId"""
    dep.detail(ano, id)


if __name__ == "__main__":
    main()