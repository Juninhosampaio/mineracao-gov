import json
import pathlib
import pydash as _
import zipfile
import io
import os
import matplotlib
import matplotlib.pyplot as plt
from datetime import datetime
from rows.utils import download_file
import pandas as p
from money.money import Money
from money.currency import Currency
import utils

import file_generators

data_path = pathlib.Path("data")


def load_file(year, download_path, encoding="utf-8"):
    filename = download_path / f"Ano-{year}.json.zip"
    if not filename.exists():
        url = f"http://www.camara.leg.br/cotas/{filename.name}"
        print(url)
        download_file(url, filename=filename, progress=True)
    try:
        zip_file = zipfile.ZipFile(filename)
    except:
        print("Arquivo corrompido. Vamos tentar baixar novamente")
        os.remove(filename)
        return load_file(year, download_path, encoding)

    return io.TextIOWrapper(zip_file.open(f"Ano-{year}.json"), encoding=encoding)


def save_restructured_json(data, json_file_out="gastos_depudatados.json"):
    data_formatted = {}
    data = json.load(data)
    filename = data_path / f"{json_file_out}"
    print("Processando...")

    for p in data['DESPESA']:
        # print(p)
        if p['nuDeputadoId'] not in data_formatted:
            data_formatted[p['nuDeputadoId']] = {}
            data_formatted[p['nuDeputadoId']]['nome'] = p['txNomeParlamentar']
            data_formatted[p['nuDeputadoId']]['sgPartido'] = p['sgPartido']
            # data_formatted[p['nuDeputadoId']]['siglaUf'] = p['siglaUf']
            data_formatted[p['nuDeputadoId']]['nuDeputadoId'] = p['nuDeputadoId']
            data_formatted[p['nuDeputadoId']]['idecadastro'] = p['idecadastro']
            data_formatted[p['nuDeputadoId']]['despesas'] = []

        # print(json.dumps(p, indent=3))
        data_formatted[p['nuDeputadoId']]['despesas'].append(
            _.omit(p, ['txNomeParlamentar', 'sgPartido', 'nuDeputadoId', 'idecadastro']))
        # i = i + 1
        # if i == 30:
        #     break
    # return dataFormatted
    print(f"Salvando json reestruturado: {filename}")

    with open(filename, 'w') as outfile:
        json.dump(data_formatted, outfile)
    # print(dataFormatted)
    return data_formatted


def to_number(s):
    return float(str(s).replace(',', '.'))


def formatNumber(n):
    return "{0:.2f}".format(n)


# Processa os dados e monta os gráficos
def proccess(data_formatted):
    for key, value in data_formatted.items():
        print(str(key) + " - " + value['nome'] + ' - ' + str(value['sgPartido']))

    n_deputado = "2295"  # input("Insira o id do deputado")
    print(">" * 50)
    # Total
    total = 0
    mensal = {}

    print(data_formatted[n_deputado]['nome'] + ' - ' + value['sgPartido'])

    for desp in data_formatted[n_deputado]['despesas']:
        total += float(to_number(desp['vlrDocumento']))

        if desp['datEmissao'] is not None:
            month = datetime.strptime(desp['datEmissao'], '%Y-%m-%d %H:%M:%S').month
            if month not in mensal: mensal[month] = 0
            mensal[month] += float(
                str(desp['vlrDocumento']).replace(',', '.'))

    for mes, valor in mensal.items():
        print("Mes " + str(mes) + ' => ' + str(valor))

    print("Total:" + str(total))


def load_or_download(year):
    # year = input("Informe o ano em que deseja consultar os dados: ");
    year = year
    download_path = data_path / "download"
    json_file_out = f"gastos_depudatados_{year}.json"

    filejson_out = data_path / f"{json_file_out}"

    for path in (data_path, download_path):
        if not path.exists():
            path.mkdir()

    reprocess = "S"
    #
    if filejson_out.exists():
        reprocess = 'N'
    #     reprocess = str(input("O arquivo já foi processado, deseja reprocessar? (S ou N): ")).upper()

    if reprocess == 'S':
        data_formatted = save_restructured_json(load_file(year, download_path), json_file_out)
    else:
        data_formatted = utils.read_json_file(filejson_out)

    categories = file_generators.items_descriptions(year)

    # print(data_formatted)
    return {
        "categories": categories,
        "data": data_formatted
    }
    # proccess(data_formatted)


def xstr(s, replace_to=''):
    if s is None:
        return replace_to
    return str(s)


def predicate_cadidato(item, body):
    nome = xstr(body['nome'], '').lower()
    partido = xstr(body['partido'], '').lower()
    return (nome != '' and xstr(item['nome']).lower().find(nome.lower()) != -1) or (partido != '' and xstr(item['sgPartido']).lower() == partido.lower())


def to_br_format(v):
    return formatNumber(v)
    # return Money(formatNumber(v), Currency.BRL).format('pt_BR')


def prepare_to_table(item, target='list_deputy'):
    if target == 'list_deputy':
        item['total'] = to_br_format(
            _.reduce_(item['despesas'], lambda total, x: total + to_number(x['vlrDocumento']), 0.0))
        return _.omit(item, 'despesas')

    if target == 'monthly_details':
        obj = _.assign({}, item)
        obj['total'] = to_br_format(obj['total'])
        obj['vlrLiquido'] = to_br_format(obj['vlrLiquido'])
        obj['vlrRestituicao'] = to_br_format(obj['vlrRestituicao'])
        obj['vlrGlosa'] = to_br_format(obj['vlrGlosa'])

        return _.omit(obj, 'despesas')


def find(year, name, party):
    result = load_or_download(year)
    data_formatted = result['data']
    body = {
        "ano": year,
        "nome": name,
        "partido": party
    }
    print(body)
    result = _.map_(_.filter_(data_formatted, lambda item: predicate_cadidato(item, body)),
                    lambda item: prepare_to_table(item))
    return p.DataFrame(result)


def detail_by_categories(deputy, categories):
    result = {}
    for desp in deputy['despesas']:
        category = utils.param_case(desp['txtDescricao'])
        # print(category)
        if category not in result:
            result[category] = {
                "categoria": desp['txtDescricao'],
                "despesas": []
            }

        result[category]["despesas"].append(desp)

    result_view = _.sort_by(_.map_(result.values(), lambda x: {
        "categoria": x['categoria'],
        "total": _.sum_by(x['despesas'], lambda val: to_number(val['vlrDocumento'])
                          )
    }), 'total')

    # print(result_view)

    columns = _.map_(result_view, lambda x: _.title_case(x['categoria']))
    values = _.map_(result_view, lambda x: x['total'])
    table = p.DataFrame([values], columns=columns)

    title = "Total dos gastos categorizados de " + _.title_case(deputy['nome'])
    print(">" * 50)
    print(title)
    print(table.T)

    table.plot.bar(title=title, figsize=(9, 6))

    plt.xlabel("Categorias")
    plt.ylabel("Totais")

    plt.show()


def detail(year, cod):
    result_data = load_or_download(year)
    data_formatted = result_data['data']
    categories = result_data['categories']

    body = {
        "nuDeputadoId": cod,
    }

    # busca o deputado com o numero passado
    deputy = _.find(data_formatted, body)

    # resultado do processamento
    result = {
        "total":
            (
            0, to_br_format(_.reduce_(deputy['despesas'], lambda total, x: total + to_number(x['vlrDocumento']), 0.0)))[
                'despesas' in deputy],
        "mensal": {}
    }

    # recupera os valores mensais
    for desp in deputy['despesas']:
        if 'datEmissao' in desp and desp['datEmissao'] is not None:
            month = datetime.strptime(desp['datEmissao'], '%Y-%m-%d %H:%M:%S').month
            if month not in result['mensal']:
                result['mensal'][month] = {
                    "mes": month,
                    "vlrLiquido": 0,
                    "vlrRestituicao": 0,
                    "vlrGlosa": 0,
                    "total": 0,
                    "despesas": []
                }

            result['mensal'][month]['vlrLiquido'] += float(
                str(desp['vlrLiquido']).replace(',', '.'))

            result['mensal'][month]['vlrRestituicao'] += float(
                str(desp['vlrRestituicao']).replace(',', '.'))

            result['mensal'][month]['vlrGlosa'] += float(
                str(desp['vlrGlosa']).replace(',', '.'))

            result['mensal'][month]['total'] += float(
                str(desp['vlrDocumento']).replace(',', '.'))

            result['mensal'][month]['despesas'].append(desp)

    monthly = _.sort_by(_.map_(result['mensal'], lambda item, key: prepare_to_table(item, 'monthly_details')), 'mes')
    footerData = [{
        "mes": "Total",
        "vlrLiquido": to_br_format(
            _.reduce_(result['mensal'], lambda total, x: total + to_number(x['vlrLiquido']), 0.0)),
        "vlrRestituicao": to_br_format(
            _.reduce_(result['mensal'], lambda total, x: total + to_number(x['vlrRestituicao']), 0.0)),
        "vlrGlosa": to_br_format(
            _.reduce_(result['mensal'], lambda total, x: total + to_number(x['vlrGlosa']), 0.0)),
        "total": result["total"]}]
    table = p.DataFrame(monthly).append(footerData)

    print("GASTOS MENSAIS")
    print(table)

    meses = ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez']

    # gráfico mensal
    graph = p.DataFrame([
        _.map_(monthly, lambda x: to_number(x['total']))
    ], columns=_.map_(monthly, lambda x: meses[int(to_number(x['mes'])) - 1])
    )
    graph.plot.bar()
    plt.xlabel("Meses")
    plt.ylabel("Totais")
    print(graph)
    # plt.show()

    detail_by_categories(deputy, categories)

    return data_formatted
